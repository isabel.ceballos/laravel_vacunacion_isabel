<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vacuna;
use App\Models\Paciente;

class VacunaController extends Controller
{
   public function index()
    {
        $vacunas=Vacuna::all();
        return view('vacunas.index', compact('vacunas'));
    }

    public function show(Vacuna $vacuna)    
    {
    	$pacientes=Paciente::all();
       return view('vacunas.show', ['vacuna'=>$vacuna,'pacientes'=>$pacientes]);
    }
}
