<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paciente;

class PacienteController extends Controller
{
	public function buscador()
    {        
        return view('pacientes.buscador');
    }
    public function buscar(Request $request){
        $busqueda=$request->busqueda;
        $paciente=Paciente::where('nombre', 'like', "%$busqueda%")->pluck('nombre');
        return response()->json($paciente);
    }
    public function vacunar(Paciente $paciente){
    	$paciente->vacunado=1;
    	$paciente->save();
    }
}
