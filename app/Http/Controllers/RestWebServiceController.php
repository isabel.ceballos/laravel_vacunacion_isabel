<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Animal;
use Illuminate\Support\Str;

class RestWebServiceController extends Controller
{
    
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return response()->json($request->especie);
        $vacuna= new Vacuna();
        $vacuna->nombre=$request->nombre;
        $vacuna->slug=Str::slug($request->nombre);       

        $vacuna->save();
        return response()->json(['mensaje'=>"Vacuna insertada."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idPaciente)
    {
        $paciente=Paciente::where('id', '=', '$idPaciente');
        $grupo=$paciente->grupo_id;
        $vacunas=$grupo->vacunas;
       return response()->json($vacunas);
    }
   
}
