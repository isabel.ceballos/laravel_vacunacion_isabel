<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Vacuna;
use App\Models\Paciente;

class Grupo extends Model
{
    use HasFactory;
    protected $table= 'grupos';
    protected $guarded=[];
    
    public function vacunas(){
        return $this->belongsToMany(Vacuna::class);
    }
    public function pacientes(){
        return $this->hasMany(Paciente::class);
    }
}
