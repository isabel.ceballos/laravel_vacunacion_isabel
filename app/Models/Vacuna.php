<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Grupo;

class Vacuna extends Model
{
    use HasFactory;
    protected $table= 'vacunas';
    protected $guarded=[];

    public function grupos(){
        return $this->belongsToMany(Grupo::class);
    }
}
