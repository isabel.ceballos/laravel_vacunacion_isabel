<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Grupo;

class Paciente extends Model
{
    use HasFactory;
    protected $table= 'pacientes';
    protected $guarded=[];
    
    public function grupos(){
    	return $this->belongsTo(Grupo::class);
    }
}
