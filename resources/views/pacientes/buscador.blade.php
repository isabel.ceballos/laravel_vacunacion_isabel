@extends('layouts.master')
@section('titulo')Buscar
@endsection
@section('contenido')
<div class="d-flex justify-content-center">
	<form class="d-flex">
        <input id="busqueda" class="form-control mr-sm-3" type="text" placeholder="Buscar" aria-label="Buscar">
      </form>
</div>
<script>
  function generateSlug (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

  $(document).ready(function(){
    $( "#busqueda" ).autocomplete({
      source: function( query, result ) {
        $.ajax( {
          type:"POST",
          url: "{{url('pacientes/busquedaAjax')}}",
          dataType: "json",
          data: {
            "_token": "{{csrf_token()}}",
            "busqueda": query['term'],
          },
          success: function( data ) {
            result( data );
          }
        });
      },
      position: {
        my: "left+0 top+8", //Mover 8px abajo.
      },
      select: function (event,ui){
        window.location=
        window.location.origin+"/pacientes/"+generateSlug(ui.item.value);
      }
      });
  });

</script>
@endsection