@extends('layouts.master')
@section('titulo')Show
@endsection
@section('contenido')
@if (session('mensaje'))
<div class="alert alert-success">
	{{session('mensaje')}}
</div>
@endif
<div class="d-flex justify-content-center">
	<div class="card" style="width: 50rem;">		
		<div class="card-body">
			<h2 class="card-title">{{$vacuna->nombre}}</h2>
			<h5 class="card-title">Pacientes NO VACUNADOS:</h5>

			<table class="table table-bordered">
				<thead class="table-warning">
					<tr>
						<th scope="col">Nombre</th>
						<th scope="col">Grupo de vacunación</th>
						<th scope="col">Prioridad</th>
					</tr>
				</thead>
				<tbody>
					
					@foreach($pacientes as $paciente)
					<tr>
						@if(!$paciente->vacunado)
						<td>{{$paciente->nombre}}</td>
						<td>{{$paciente->grupos->nombre}}</td>
						<td>{{$paciente->grupos->prioridad}}</td>
						@endif
						@endforeach
					</tr>
				</tbody>
			</table>
			<h5 class="card-title">Pacientes vacunados:</h5>
			<div class="card-group">
				@foreach($vacuna->grupos as $grupo)				
				<div class="card">
					<div class="card-body">
						<h4 style="min-height:45px;margin:5px 0 10px 0">{{$grupo->nombre}}</h4>
						@foreach($grupo->pacientes as $paciente)
						@if($paciente->vacunado)
						<ul>
							<li>{{$paciente->nombre}} ({{$paciente->fechaVacuna}})</li>
						</ul>
						@endif

						@endforeach
					</div>
				</div>
				
				@endforeach
			</div>
		</div>
	</div>
	@endsection