@extends('layouts.master')
@section('titulo')Index
@endsection
@section('contenido')
<div class="d-flex justify-content-center">
	@foreach($vacunas as $clave=> $vacuna)
	<div class="card-group">
		<div class="card">		
			<a href="{{ route('vacunas.show' , $vacuna) }}">				
				<div class="card-body">
					<h4 style="min-height:45px;margin:5px 0 10px 0">{{$vacuna->nombre}}
					</h4>					
				</div>
			</a>
			<p class="card-text"><strong>Posibles grupos de vacunación:</strong></p>
			@foreach($vacuna->grupos as $grupo)					
					<p> {{ $grupo->nombre }} </p>
				@endforeach	
			
		</div>
	</div>
	@endforeach
</div>
@endsection